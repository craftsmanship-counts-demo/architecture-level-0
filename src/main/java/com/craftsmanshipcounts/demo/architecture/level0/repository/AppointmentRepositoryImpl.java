package com.craftsmanshipcounts.demo.architecture.level0.repository;

import com.craftsmanshipcounts.demo.architecture.level0.domain.Appointment;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AppointmentRepositoryImpl
	implements AppointmentRepository
{
	private final NamedParameterJdbcTemplate jdbcTemplate;

	public AppointmentRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Integer create(Appointment appointment) {
		return null;
	}

	@Override
	public Appointment read(Integer id) {
		return null;
	}

	@Override
	public List<Appointment> read() {
		return null;
	}

	@Override
	public boolean update(Integer id, Appointment appointment) {
		return false;
	}

	@Override
	public boolean delete(Integer id) {
		return false;
	}
}
