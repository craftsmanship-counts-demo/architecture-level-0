package com.craftsmanshipcounts.demo.architecture.level0.domain;

import com.craftsmanshipcounts.demo.architecture.level0.validation.Group;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class Attendee
{
	@Null(groups = Group.OnCreate.class)
	@NotNull(groups = Group.OnUpdate.class)
	@Positive
	private Integer id;

	@NotBlank
	@Size(min = 2, max = 50)
	private String name;

	@NotNull
	@Email
	private String email;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, DomainDefaults.DEFAULT_TO_STRING_STYLE);
	}
}
