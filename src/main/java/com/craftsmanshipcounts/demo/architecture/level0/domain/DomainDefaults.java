package com.craftsmanshipcounts.demo.architecture.level0.domain;

import org.apache.commons.lang3.builder.ToStringStyle;

public class DomainDefaults
{
	public static final ToStringStyle DEFAULT_TO_STRING_STYLE = ToStringStyle.DEFAULT_STYLE;
}
