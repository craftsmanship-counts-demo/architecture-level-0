package com.craftsmanshipcounts.demo.architecture.level0.controller;

import com.craftsmanshipcounts.demo.architecture.level0.domain.Appointment;
import com.craftsmanshipcounts.demo.architecture.level0.service.AppointmentService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping(value = "/appointment", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
public class AppointmentController
{
	private final AppointmentService service;

	public AppointmentController(AppointmentService service) {
		this.service = service;
	}

	@PostMapping
	public Appointment create(@RequestBody Appointment appointment) {
		Integer id = service.create(appointment);
		return read(id);
	}

	@GetMapping("/{id}")
	public Appointment read(@PathVariable Integer id) {
		return service.read(id);
	}

	@GetMapping
	public List<Appointment> read() {
		return service.read();
	}

	@PutMapping("/{id}")
	public Appointment update(@PathVariable Integer id, @RequestBody Appointment appointment) {
		service.update(id, appointment);

		return read(id);
	}

	@DeleteMapping("/{id}")
	public Boolean delete(@PathVariable Integer id) {
		return service.delete(id);
	}
}
