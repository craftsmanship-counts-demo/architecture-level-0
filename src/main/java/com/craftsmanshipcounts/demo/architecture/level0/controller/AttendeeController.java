package com.craftsmanshipcounts.demo.architecture.level0.controller;

import com.craftsmanshipcounts.demo.architecture.level0.domain.Attendee;
import com.craftsmanshipcounts.demo.architecture.level0.service.AttendeeService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping(value = "/appointment/{appointmentId}/attendee", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
public class AttendeeController
{
	private final AttendeeService service;

	public AttendeeController(AttendeeService service) {
		this.service = service;
	}

	@PostMapping
	public Attendee create(@RequestBody Attendee appointment) {
		Integer id = service.create(appointment);
		return read(id);
	}

	@GetMapping("/{id}")
	public Attendee read(@PathVariable Integer id) {
		return service.read(id);
	}

	@GetMapping
	public List<Attendee> read() {
		return service.read();
	}

	@PutMapping("/{id}")
	public Attendee update(@PathVariable Integer id, @RequestBody Attendee appointment) {
		service.update(id, appointment);

		return read(id);
	}

	@DeleteMapping("/{id}")
	public Boolean delete(@PathVariable Integer id) {
		return service.delete(id);
	}
}
