package com.craftsmanshipcounts.demo.architecture.level0.repository;

import com.craftsmanshipcounts.demo.architecture.level0.domain.Attendee;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AttendeeRepositoryImpl
	implements AttendeeRepository
{
	private final NamedParameterJdbcTemplate jdbcTemplate;

	public AttendeeRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Integer create(Attendee attendee) {
		return null;
	}

	@Override
	public Attendee read(Integer id) {
		return null;
	}

	@Override
	public List<Attendee> read() {
		return null;
	}

	@Override
	public boolean update(Integer id, Attendee attendee) {
		return false;
	}

	@Override
	public boolean delete(Integer id) {
		return false;
	}
}
