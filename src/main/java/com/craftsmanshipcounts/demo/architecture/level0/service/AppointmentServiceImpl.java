package com.craftsmanshipcounts.demo.architecture.level0.service;

import com.craftsmanshipcounts.demo.architecture.level0.domain.Appointment;
import com.craftsmanshipcounts.demo.architecture.level0.repository.AppointmentRepository;
import com.craftsmanshipcounts.demo.architecture.level0.validation.Group;
import org.springframework.stereotype.Service;

import javax.validation.Validation;
import javax.validation.Validator;
import java.util.List;

@Service
public class AppointmentServiceImpl
	implements AppointmentService
{
	private final Validator validator;
	private final AppointmentRepository repository;

	public AppointmentServiceImpl(AppointmentRepository repository) {
		this.validator = Validation.buildDefaultValidatorFactory().getValidator();
		this.repository = repository;
	}

	@Override
	public Integer create(Appointment appointment) {
		validator.validate(appointment, Group.OnCreate.class);
		return repository.create(appointment);
	}

	@Override
	public Appointment read(Integer id) {
		return repository.read(id);
	}

	@Override
	public List<Appointment> read() {
		return repository.read();
	}

	@Override
	public boolean update(Integer id, Appointment appointment) {
		validator.validate(appointment, Group.OnUpdate.class);
		return repository.update(id, appointment);
	}

	@Override
	public boolean delete(Integer id) {
		return repository.delete(id);
	}
}
