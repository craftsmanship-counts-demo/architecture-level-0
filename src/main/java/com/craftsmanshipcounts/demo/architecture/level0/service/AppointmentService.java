package com.craftsmanshipcounts.demo.architecture.level0.service;

import com.craftsmanshipcounts.demo.architecture.level0.domain.Appointment;

import java.util.List;

public interface AppointmentService
{
	Integer create(Appointment appointment);
	Appointment read(Integer id);
	List<Appointment> read();
	boolean update(Integer id, Appointment appointment);
	boolean delete(Integer id);
}
