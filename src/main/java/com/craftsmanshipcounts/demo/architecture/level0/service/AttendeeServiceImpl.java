package com.craftsmanshipcounts.demo.architecture.level0.service;

import com.craftsmanshipcounts.demo.architecture.level0.domain.Attendee;
import com.craftsmanshipcounts.demo.architecture.level0.repository.AttendeeRepository;
import com.craftsmanshipcounts.demo.architecture.level0.validation.Group;
import org.springframework.stereotype.Service;

import javax.validation.Validation;
import javax.validation.Validator;
import java.util.List;

@Service
public class AttendeeServiceImpl
	implements AttendeeService
{
	private final Validator validator;
	private final AttendeeRepository repository;

	public AttendeeServiceImpl(AttendeeRepository repository) {
		this.validator = Validation.buildDefaultValidatorFactory().getValidator();
		this.repository = repository;
	}

	@Override
	public Integer create(Attendee attendee) {
		validator.validate(attendee, Group.OnCreate.class);
		return repository.create(attendee);
	}

	@Override
	public Attendee read(Integer id) {
		return repository.read(id);
	}

	@Override
	public List<Attendee> read() {
		return repository.read();
	}

	@Override
	public boolean update(Integer id, Attendee attendee) {
		validator.validate(attendee, Group.OnUpdate.class);
		return repository.update(id, attendee);
	}

	@Override
	public boolean delete(Integer id) {
		return repository.delete(id);
	}
}
