package com.craftsmanshipcounts.demo.architecture.level0.repository;

import com.craftsmanshipcounts.demo.architecture.level0.domain.Attendee;

import java.util.List;

public interface AttendeeRepository
{
	Integer create(Attendee attendee);
	Attendee read(Integer id);
	List<Attendee> read();
	boolean update(Integer id, Attendee attendee);
	boolean delete(Integer id);
}
