drop schema if exists public;

drop table if exists contact;
create table contact
(
    id serial not null
        constraint contact_pk
            primary key,
    full_name varchar(100),
    email varchar(255) not null
);

drop table if exists appointment;
CREATE TABLE appointment
(
    id serial NOT NULL
        constraint appointment_pk
            primary key,
    starts_at timestamp(0) with time zone NOT NULL,
    duration time NOT NULL,
    summary varchar(255) NOT NULL,
    description text
);

drop table if exists contact_type;
create table contact_type
(
    id int not null
        constraint contact_type_pk
            primary key,
    name varchar(25) not null
);

insert into contact_type (id, name)
values (1, 'Organizer')
     , (2, 'Attendee');

drop table if exists appointment_contact;
create table appointment_contact
(
    appointment_id int not null,
    contact_id int not null,
    contact_type_id int not null,

    constraint appointment_contact_pk
        primary key (appointment_id, contact_id),

    constraint appointment_appointment_id_fk
        foreign key (appointment_id) references appointment (id),

    constraint appointment_contact_id_fk
        foreign key (contact_id) references contact (id),

    constraint appointment_contact_type_id_fk
        foreign key (contact_type_id) references contact_type (id)
);
